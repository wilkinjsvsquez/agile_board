Add this packages

1. dotnet add package Microsoft.EntityFrameworkCore.SqlServer
2. dotnet add package Microsoft.EntityFrameworkCore.InMemory
3. dotnet add package Microsoft.EntityFrameworkCore.Design
4. dotnet add package Npgsql
5. dotnet add package EFCore.NamingConventions
6. dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL

dotnet ef migrations add InitialMigration
dotnet ef database update