using System.Collections.Generic;
using System.Threading.Tasks;
using Agile_BoardAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Agile_BoardAPI
{
    [ApiController]
    [Route("[controller]")]
    public class DutiesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public DutiesController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        //https://localhost:5001/duties
        public async Task<ActionResult<IEnumerable<Duty>>> getDuties()
        {
            return await _context.duties.ToListAsync();
        }

        //https://localhost:5001/duties/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Duty>> getDuty(long id)
        {
            var duty = await _context.duties.FindAsync(id);
            if (duty == null)
            {
                NotFound();
            }
            return duty;
        }

        [HttpPost]
        //https://localhost:5001/duties
        public async Task<ActionResult<Duty>> postTask(Duty duty)
        {
            _context.duties.Add(duty);
            await _context.SaveChangesAsync();
            return CreatedAtAction("getDuties", new { id = duty.id }, duty);
        }

        //https://localhost:5001/duties/id
        [HttpPut("{id}")]
        public async Task<ActionResult<Duty>> updateDuty(long id, Duty duty)
        {
            if (id != duty.id)
            {
                return duty;
            }
            _context.Entry(duty).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return CreatedAtAction("getDuty", new { id = duty.id }, duty);
        }

        //https://localhost:5001/duties/id
        [HttpDelete("{id}")]
        public async Task<ActionResult<Duty>> deleteDuty(long id)
        {
            var duty = await _context.duties.FindAsync(id);
            if (duty == null)
            {
                return NotFound();
            }
            _context.duties.Remove(duty);
            await _context.SaveChangesAsync();
            return duty;
        }
    }
}