using System.Collections.Generic;
using System.Threading.Tasks;
using Agile_BoardAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Agile_BoardAPI
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public UsersController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        //https://localhost:5001/Users
        public async Task<ActionResult<IEnumerable<User>>> getUsers()
        {
            return await _context.users.ToListAsync();
        }

        //https://localhost:5001/users/id
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> getUser(long id)
        {
            var user = await _context.users.FindAsync(id);
            if (user == null)
            {
                NotFound();
            }
            return user;
        }

        [HttpPost]
        //https://localhost:5001/users
        public async Task<ActionResult<User>> postUser(User user)
        {
            _context.users.Add(user);
            await _context.SaveChangesAsync();
            return CreatedAtAction("getUsers", new { id = user.id }, user);
        }

        //https://localhost:5001/users/id
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> updateUser(long id, User user)
        {
            if (id != user.id)
            {
                return user;
            }
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return CreatedAtAction("getUser", new { id = user.id }, user);
        }

        //https://localhost:5001/users/id
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> deleteUser(long id)
        {
            var user = await _context.users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            _context.users.Remove(user);
            await _context.SaveChangesAsync();
            return user;
        }
    }
}