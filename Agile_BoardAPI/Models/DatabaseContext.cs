using Microsoft.EntityFrameworkCore;

namespace Agile_BoardAPI.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        public DbSet<Project> projects { get; set; }
        public DbSet<Duty> duties { get; set; }
        public DbSet<DutyState> states { get; set; }
        public DbSet<User> users { get; set; }
    }
}