using System.Collections.Generic;

namespace Agile_BoardAPI
{
    public class Project
    {
        public long id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isActive { get; set; }
        public long boardId { get; set; }
        public AgileBoard agileBoard { get; set; }
        public List<Duty> duties { get; set; }
    }
}