
namespace Agile_BoardAPI
{
    public class Duty
    {
        public long id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public long projectId { get; set; }
        public Project project { get; set; }
        // public long dutyStateID { get; set; }
        // public DutyState DutyState { get; set; }
        // public long userId { get; set; }
        // public User user { get; set; }
    }
}