namespace Agile_BoardAPI
{
    public class AgileBoard
    {
        public long id { get; set; }
        public long projectId { get; set; }
        public Project project { get; set; }
    }
}